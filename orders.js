var faker = require('faker')

function generateOrders() {
  var orders = []
  for(id=0; id<500; id++){
    var orderNumber = faker.random.number();
    var supplierName = faker.name.firstName();

    orders.push({
      "id": id,
      "orderNumber": orderNumber,
      supplier: {
        "supplierName": supplierName
      }
    })
  }
  return {"orders": orders}

}

module.exports = generateOrders
